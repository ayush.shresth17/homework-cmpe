def toUpperCase():
    letter = input("enter a text: ")
    i = ord(letter) - 32
    letter = chr(i)
    return letter

def toLowerCase():
    letter = input("enter a text: ")
    i = ord(letter) + 32
    letter = chr(i)
    return letter

def checkAlphabet():
    letter = input("enter a text: ")
    if((ord(letter) >= 65 and ord(letter) <= 90) or (ord(letter) >= 97 and ord(letter) <= 122)):
        print(True)
    else:
        print(False)

def checkDigit():
    letter = input("enter a text: ")
    if((ord(letter) >= 48 and ord(letter) <= 57)):
        print(True)
    else:
        print(False)

def checkSpecialChar():
    letter = input("enter a text: ")
    if((ord(letter) >= 32 and ord(letter) <=47)
        or (ord(letter) >= 58 and ord(letter) <=64)
        or (ord(letter) >= 91 and ord(letter) <=96)
        or (ord(letter) >= 123 and ord(letter) <=126)):
        print(letter, "is a special character")
    else:
        print(letter, "is not a special character")


def main():
    
    text = toUpperCase()
    print(text)
    print("------------------")
    text2 = toLowerCase()
    print(text2)
    print("------------------")
    text3 = checkAlphabet()
    print("------------------")
    text4 = checkDigit()
    print("------------------")
    text5 = checkSpecialChar()
    

main()

