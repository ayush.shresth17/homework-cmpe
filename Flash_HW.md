## FLASH MEMORY HOMEWORK

> Explain the different types of memory.

### NOR FLASH MEMORY
* It acts like a NOR gate. When one of the bit lines is high, the corresponding storage transistor acts to pull the output bit line low. 
* Non-Volatile storage.
* Faster to read than NAND Flash Memory.
* Slower to erase and write new data.
* More Expensive.
* Used in Low capacity and high reliability applications like- Mobile phones, medical instruments.

### NAND FLASH MEMORY
* It acts like a NAND GATE. The bit line is pulled low only if all the line are pulled high. 
* Non Volatile storage.
* Faster to erase and write new data.
* Slower to read than NOR Flash Memory.
* Higher memory capacity.
* Less expensive. 
* Used in high capacity applications like- Cameras and pendrives. 

>  Outline its limitations.

* Memory Wear: It has finite number of program-erase cycles (P/E cycles) Most commercially available flash memory has around 1000000 P/E cycles. 


> Why is serial flash memory preferred in embedded systems?

Flash memory is a long-life and non-volaitle storage chip. It can keep stored data and information even when the power is turn-off. It is highly efficient and cheaper which is why it is widely used in embedded systems. 

> Provide information about how memory is re-written

* Flash memory erases data in units called blocks and rewrites data at the byte level. Once data is written to a block, it cannot be re-written unles the data there is erased. Flash memory erases used space by over-writing what is already there, which consumes one of writes that cell can have and reduces it usable life. 

* it is similar to a pencil paper anaolgy which if you keep on writing and erasing a same spot of paper, then it will get weared out. 

> Is writing to a flash memory straightforward ? How will you set/clear a particular bit of flash memory ?

* Writing in flash memory is not straightforward because you have to first erase the bit that is occupied, and as the data is erased in blocks which consists of multiple bytes therefore, inorder to clear a particular bit you will have to clear the whole block. 

